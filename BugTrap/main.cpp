#include "main.h"
#include <SRHook.hpp>
#include <dbghelp.h>
#include <psapi.h>

struct SymInfo {
	const char *name;
	size_t		addr;
};

typedef SymInfo ( *gameSym_t )( size_t addr );

typedef void( __stdcall *BT_SetAppName_t )( const char *pszAppName );
typedef void( __stdcall *BT_SetFlags_t )( unsigned long dwFlags );
typedef void( __stdcall *BT_InstallSehFilter_t )();

gameSym_t gameSym = nullptr;
WINBOOL __stdcall SymFromAddrHook( HANDLE process, DWORD64 address, PDWORD64 displacement, PSYMBOL_INFO symbol ) {
	if ( !gameSym ) {
		return SymFromAddr( process, address, displacement, symbol );
	}
	if ( process != GetCurrentProcess() ) {
		return SymFromAddr( process, address, displacement, symbol );
	}
	if ( symbol == nullptr ) {
		return SymFromAddr( process, address, displacement, symbol );
	}

	unsigned long size = 14'383'616;
	MODULEINFO	  mi;
	memset( &mi, 0, sizeof( MODULEINFO ) );
	if ( GetModuleInformation( process, GetModuleHandleA( NULL ), &mi, sizeof( MODULEINFO ) ) && mi.SizeOfImage )
		size = mi.SizeOfImage;
	if ( address < 0x00400000 || address > 0x00400000 + size ) {
		return SymFromAddr( process, address, displacement, symbol );
	}

	SymFromAddr( process, address, displacement, symbol );
	auto gi = gameSym( address );
	strncpy( symbol->Name, gi.name, symbol->MaxNameLen );
	symbol->NameLen = strlen( gi.name );

	return TRUE;
}

AsiPlugin::AsiPlugin() : SRDescent() {
	// Constructor
	auto bt = LoadLibraryA( "BugTrap.dll" );
	if ( bt && bt != INVALID_HANDLE_VALUE ) {
		auto gs = LoadLibraryA( "GameSyms.dll" );
		if ( gs && gs != INVALID_HANDLE_VALUE ) {
			gameSym = (gameSym_t)GetProcAddress( gs, "gameSym" );
			static SRHook::Hook<> symHook{ size_t( bt ) + 0xE2CE, 16 };
			symHook.install();
			symHook.onBefore += [] {
				*(size_t *)( symHook.cpu.ESI + 0x6FC ) = symHook.cpu.EAX;
				symHook.skipOriginal();
				symHook.cpu.EAX = (size_t)&SymFromAddrHook;
			};
		}

		auto BT_SetAppName		 = (BT_SetAppName_t)GetProcAddress( bt, "BT_SetAppName" );
		auto BT_SetFlags		 = (BT_SetFlags_t)GetProcAddress( bt, "BT_SetFlags" );
		auto BT_InstallSehFilter = (BT_InstallSehFilter_t)GetProcAddress( bt, "BT_InstallSehFilter" );

		BT_SetAppName( "GTA: San Andreas" );
		BT_SetFlags( 0x001 | 0x020 | 0x040 );
		BT_InstallSehFilter();
	}
}

AsiPlugin::~AsiPlugin() {
	// Destructor
}
