#include "loader.h"
#include "../main.h"
#include <SRHook.hpp>
#include <d3d9/proxydirectx.h>
#include <regex>

std::string_view PROJECT_NAME = _PRODJECT_NAME;
stGlobalHandles	 g_handle;
stGlobalClasses	 g_class;
stGlobalPVars	 g_vars;

static AsiPlugin *pAsiPlugin = static_cast<AsiPlugin *>( nullptr );

CALLHOOK GameLoop() {
	static bool hooked = false;
	if ( hooked ) return;
	hooked = true;

	pAsiPlugin = new AsiPlugin();
}

BOOL APIENTRY DllMain( HMODULE, DWORD dwReasonForCall, LPVOID ) {
	static SRHook::Hook<> gameloopHook{ 0x748DA3, 6 };

	if ( dwReasonForCall == DLL_PROCESS_ATTACH ) {
		gameloopHook.install();
		gameloopHook.onBefore += GameLoop;
	} else if ( dwReasonForCall == DLL_PROCESS_DETACH ) {
		gameloopHook.remove();
		delete pAsiPlugin;
		pAsiPlugin = nullptr;
	}

	return TRUE;
}

int MessageBox( std::string_view text, std::string_view title, UINT type ) {
	return MessageBoxA( g_vars.hwnd, text.data(), title.data(), type );
}
