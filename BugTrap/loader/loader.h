#ifndef LOADER_H
#define LOADER_H

#include <base.h>

/**
 * \brief Выводит окно с сообщениемю
 * \detail Является оберткой над MessageBoxA, для более удобного вывода сообщений.
 * \param[in] text Текст сообщения.
 * \param[in] title Заголовок окна с сообщением.
 * \param[in] type Тип окна.
 * \return код завершения окна (нажатая кнопка).
 */
int MessageBox( std::string_view text, std::string_view title = PROJECT_NAME, UINT type = MB_OK );

#endif // LOADER_H
